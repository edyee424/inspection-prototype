import React, { Fragment } from 'react';
// import logo from './logo.svg';
// import './App.css';
import { withRouter, Redirect } from 'react-router-dom';
// import Login from './components/pages/login/login';
// import Home from './components/pages/home/home';
// import { useTranslation } from 'react-i18next';
import db from './services/db';

const App = () => {
  // const  [t, i18n] = useTranslation();
  let isAuthenticated: boolean = (localStorage["authtoken"] !== undefined);

  // Initialize DB
  // initDB();

  // db.table('serviceOrders').add({ serNumber: 'SER00001', lineNumber: '1'}).then((id) => {
  //   console.log("Inserted! Return ID: ", id);
  // })

  return (
      <Fragment>
      {isAuthenticated ? <Redirect to="/Home" /> : <Redirect to="/Login" /> }
      </Fragment>
  );
  // return (
  //   <div className="App">
  //     <header className="App-header">
  //       <img src={logo} className="App-logo" alt="logo" />
  //       <p>
  //         Edit <code>src/App.tsx</code> and save to reload.
  //       </p>
  //       <a
  //         className="App-link"
  //         href="https://reactjs.org"
  //         target="_blank"
  //         rel="noopener noreferrer"
  //       >
  //         Learn React
  //       </a>
  //     </header>
  //   </div>
  // );
};

export default withRouter(App);
