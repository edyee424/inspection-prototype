import Dexie from 'dexie';

/*y
export interface ISORpt {
    id?: number;
    soID: number; // IServiceOrder id
    rptID?: number; // iReport id
    status: string; // "N", "I", etc.
    noReport?: boolean; // Applicable to OS0001 only
    incomplete: boolean;
}

export interface IDoc {
    id?: number;
    type: string; // "PO", "SI", "image"
    filename: string; // Actual or generated file name
    file: string; // Base64 encoded
}

export interface IReport {
    id?: number;

    // New occupancy code
    occupancyCode: string; // Applicable to OS0006 only

    // LPR
    hasLPR: boolean;

    // Contact
    contactName: string;
    contactPhone: string;
    contactAltPhone: string;
    contactEmail: string;
    contactNoEmailNotes?: string;

    // Location data
    ldAge: number; // 0, 1, 2, 3
    ldYearBuilt: string; // YYYY
    ldBuildingNotes?: string;
    ldStories?: number; // Applicable to OS0007 and OS0009 only
    ldArea?: number; // Applicable to OS0007 and OS0009 only
    ldLatitude?: number; // Applicable to OS0007 and OS0009 only
    ldLongitude?: number; // Applicable to OS0007 and OS0009 only
    ldConstructionType?: string; // Applicable to OS0007 and OS0009 only

    // Duration
    durReporting?: number; // Applicable to OS0007 and OS0009 only
    durInspection?: number; // Applicable to OS0007 and OS0009 only

    alternativeEnergyComments?: string; // Applicable to OS0006 only
    generalComments?: string;
    noPictureComments?: string;

    // generalObservations: IGeneralObservation[];
    // altEnergies: IAlternativeEnergy[];
    // photos: IReportPhoto[];

    // electricalSystems?: IElectricalSystem[]; // Applicable to OS0006 only
    // envAgencies?: IEnvAgnecy[]; // Applicable to OS0006 only
    // constructionClasses?: IConstructionClassification; // Applicable to OS0007 and OS0009 only
    // secondaryModifiers?: ISecondaryModifier[]; // Applicable to OS0007 and OS0009 only
    // buildingProtection?: IBuildingProtection; // Applicable to OS0009 only
    // dryClean?: IDryClean; // Applicable to OS0031 only
}

export interface IGeneralObservation {
    id?: number;

    rptId: number; // IReport ID
    key: string;
    state: boolean;
    comments?: boolean;
    condition?: number; // Applicable to OS0008 only
    boiler?: boolean; // Applicable to OS0008 cluttered only
    electrical?: boolean; // Applicable to OS0008 cluttered only
    other?: boolean; // Applicable to OS0008 cluttered only
}

export interface IAlternativeEnergy {
    id?: number;

    rptId: number; // IReport ID
    key: string;
    state: boolean;
}

export interface IReportPhoto {
    id?: number;

    rptId: number; // IReport ID
    docId: number; // IDoc ID
    caption?: string;
}

// Applicable to OS0006 only
export interface IElectricalSystem {
    id?: number;

    rptId: number; // IReport ID
    key: string;
    answer?: string;
    comments?: string;
    score?: number;
}

// Applicable to OS0006 only
export interface IEnvAgnecy {
    id?: number;

    rptId: number; // IReport ID
    key: string;
    state: boolean;
}

// Applicable to OS0007 and OS0009 only
export interface IConstructionClassification {
    id?: number;

    rptId: number; // IReport ID
    roofSupport?: number;
    roofDeck?: number;
    perimeterSupport?: number;
    internalSupport?: number;

    // Floor support
    dimensionalLumber: boolean;
    woodTimbers: boolean;
    steel: boolean;
    protectedSteel: boolean;
    concrete: boolean
}

// Applicable to OS0007 and OS0009 only
export interface ISecondaryModifier {
    id?: number;

    rptId: number; // IReport ID
    key: string;
    value?: number;
    comments?: string
}

// Applicable to OS0009 only
export interface IBuildingProtection {
    id?: number;

    rptId: number; // IReport ID
    sprinklered?: number;
    superNotification?: number;
    fireSystem?: number;
    fireHydrant?: number;

    // Supervisor monitor
    sprinklerSystem: boolean;
    securityAlarm: boolean;
    smokeDetector: boolean;
    none: boolean
}

// Applicable to OS0031 only
export interface IDryClean {
    id?: number;

    rptId: number; // IReport ID
    waterTreatment: IDryCleanItem;
    boilerLogs: IDryCleanItem;
    maintenance: IDryCleanItem;
    surgeSuppression: IDryCleanItem;
    over20: IDryCleanOver20;
}

// Applicable to OS0031 only
export interface IDryCleanItem {
    id?: number;

    dyrCleanId: number; // IDryClean ID
    value?: number;
    comments?: string
}

// Applicable to OS0031 only
export interface IDryCleanOver20 {
    id?: number;

    dyrCleanId: number; // IDryClean ID
    electrical: boolean;
    prodEquipment: boolean;
    boilers: boolean;
    none: boolean;
    comments?: string
}

export interface IServiceOrder {
    id?: number;
    businessUnit: string; // "CSC01"
    serNumber: string; // "SER#####"
    lineNumber: string; // "###"
    onsightType: string; // "OS####"
    onsightDesc: string; // OnSight type textual description
    soStatus: string; // "OPA", "CFM", "COM", etc.

    promiseTime: string; // HH:mm, "AM", "PM", "Anytime", etc.
    startDate: string; // YYYY-MM-DD
    startTime: string; // HH:mm
    endDate: string; // YYYY-MM-DD
    endTime: string; // HH:mm

    // Client company
    clientCoId: string; // ID
    clientCoName: string; // Name

    // Address
    addrStreet: string; 
    addrCity: string;
    addrState: string;
    addrZip: string;

    // Contact
    contactName: string;
    contactPhone: string;
    contactAltPhone: string;
    contactEmail: string;

    // Trunkwork
    isTrunkwork: boolean;
    twAssocSerNumber?: string; // Associated service order
    twDate?: string; // YYYY-MM-DD

    // Inspector
    inspectorId: string;
    inspectorFirstName: string;
    inspectorLastName: string;

    // Prior OnSight Report
    hasPO?: boolean;
    poTechkey: string;
    poFileId?: number; // IDoc ID

    // Special instruction
    hasSI: boolean;
    siFileId?: number; // IDoc ID

    // Occupancy code
    occupancyCode: string;
}

export interface ISOInteractionLog {
    id?: number;

    soID: number; // IServiceOrder ID
    iLogId: string;
    iLogDate: Date; // YYYY-MM-DDTHH:mm:ss.SSSSSSZZ
    iLogStatus: string; // Empty string or something specific
    iLogDetails: string;
}

export interface IServiceOrderSmall {
    id?: number;
    serNumber: string; // "SER#####"
    lineNumber: string; // "###"
    onsightType: string; // "OS####"
    onsightDesc: string; // OnSight type textual description

    promiseTime: string; // HH:mm, "AM", "PM", "Anytime", etc.
    startDate: string; // YYYY-MM-DD
    startTime: string; // HH:mm
    endDate: string; // YYYY-MM-DD
    endTime: string; // HH:mm

}

export class OnSightDatabase extends Dexie {
    // so: Dexie.Table<IServiceOrder, number>;
    // rpt: Dexie.Table<IReport, number>;
    // soRptRef: Dexie.Table<ISORpt, number>;
    // doc: Dexie.Table<IDoc, number>;
    // genObsrv: Dexie.Table<IGeneralObservation, number>;
    // altEnergy: Dexie.Table<IAlternativeEnergy, number>;
    // photo: Dexie.Table<IReportPhoto, number>;
    // elecSystem: Dexie.Table<IElectricalSystem, number>;
    // envAgency: Dexie.Table<IEnvAgnecy, number>;
    // constructionClass: Dexie.Table<IConstructionClassification, number>;
    // secModifier: Dexie.Table<ISecondaryModifier, number>;
    // buildingProtection: Dexie.Table<IBuildingProtection, number>;
    // dryClean: Dexie.Table<IDryClean, number>;
    // dryCleanItem: Dexie.Table<IDryCleanItem, number>;
    // dryCleanOver20: Dexie.Table<IDryCleanOver20, number>;
    soSmall: Dexie.Table<IServiceOrderSmall, number>;

    constructor() {
        super("OnSight");

        console.log("Instantiating database");

        this.version(1).stores({
            // so: '++id, [serNumber + lineNumber]',
            // rpt: '++id',
            // soRptRef: '++id',
            // doc: '++id',
            // genObsrv: '++id, rptId, key',
            // altEnergy: '++id, rptId, key',
            // photo: '++id, rptId, docId',
            // elecSystem: '++id, rptId, key',
            // envAgency: '++id, rptId, key',
            // constructionClass: '++id, rptId',
            // secModifier: '++id, rptId, key',
            // buildProtection: '++id, rptId',
            // dryClean: '++id, rptId',
            // dryCleanItem: '++id, dyrCleanId',
            // dryCleanOver20: '++id, dyrCleanId'
            soSmall: '++id, [serNumber + lineNumber]'
        });

        // this.so = this.table("serviceOrder");
        // this.rpt = this.table("report");
        // this.soRptRef = this.table("soRptRef");
        // this.doc = this.table("doc");
        // this.genObsrv = this.table("generalObservation");
        // this.altEnergy = this.table("alternativeEnergy");
        // this.photo = this.table("photo");
        // this.elecSystem = this.table("electricalSystem");
        // this.envAgency = this.table("envAgency");
        // this.constructionClass = this.table("constructionClassification");
        // this.secModifier = this.table("secondaryModifiers");
        // this.buildingProtection = this.table("buildingProtection");
        // this.dryClean = this.table("dryClean");
        // this.dryCleanItem = this.table("dryCleanItem");
        // this.dryCleanOver20 = this.table("dryCleanOver20");

        // this.so.mapToClass(ServiceOrder);
        this.soSmall.mapToClass(ServiceOrderSmall);
    }
}

export const db = new OnSightDatabase();

export class ServiceOrderSmall implements IServiceOrderSmall {
    id: number;
    serNumber: string; // "SER#####"
    lineNumber: string; // "###"
    onsightType: string; // "OS####"
    onsightDesc: string; // OnSight type textual description

    promiseTime: string; // HH:mm, "AM", "PM", "Anytime", etc.
    startDate: string; // YYYY-MM-DD
    startTime: string; // HH:mm
    endDate: string; // YYYY-MM-DD
    endTime: string; // HH:mm

    constructor(serNumber: string, lineNumber: string, onsightType: string, onsightDesc: string, promiseTime: string, startDate: string, startTime: string, endDate: string, endTime: string, id?: number ) {
        this.serNumber = serNumber;
        this.lineNumber = lineNumber;
        this.onsightType = onsightType;
        this.onsightDesc = onsightDesc;
        this.promiseTime = promiseTime;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endDate = endDate;
        this.endTime = endTime;
        if (id) this.id = id;
    }

    save() {
        return db.transaction('rw', db.soSmall, () => {
            db.soSmall.put(new ServiceOrderSmall(this.serNumber, this.lineNumber, this.onsightType, this.onsightDesc, this.promiseTime, this.startDate, this.startTime, this.endDate, this.endTime, this.id)).then(id => {
                this.id = id;
            });
        });
    }
}

export class ServiceOrder implements IServiceOrder {
    id?: number;
    businessUnit: string; // "CSC01"
    serNumber: string; // "SER#####"
    lineNumber: string; // "###"
    onsightType: string; // "OS####"
    onsightDesc: string; // OnSight type textual description
    soStatus: string; // "OPA", "CFM", "COM", etc.

    promiseTime: string; // HH:mm, "AM", "PM", "Anytime", etc.
    startDate: string; // YYYY-MM-DD
    startTime: string; // HH:mm
    endDate: string; // YYYY-MM-DD
    endTime: string; // HH:mm

    // Client company
    clientCoId: string; // ID
    clientCoName: string; // Name

    // Address
    addrStreet: string; 
    addrCity: string;
    addrState: string;
    addrZip: string;

    // Contact
    contactName: string;
    contactPhone: string;
    contactAltPhone: string;
    contactEmail: string;

    // Interaction log
    iLogId?: string;
    iLogDate?: Date; // YYYY-MM-DDTHH:mm:ss.SSSSSSZZ
    iLogStatus?: string; // Empty string or something specific
    iLogDetails?: string;

    // Trunkwork
    isTrunkwork: boolean;
    twAssocSerNumber?: string; // Associated service order
    twDate?: string; // YYYY-MM-DD

    // Inspector
    inspectorId: string;
    inspectorFirstName: string;
    inspectorLastName: string;

    // Prior OnSight Report
    hasPO?: boolean;
    poTechkey: string;
    poFileId?: number; // IDoc ID

    // Special instruction
    hasSI: boolean;
    siFileId?: number; // IDoc ID

    // Occupancy code
    occupancyCode: string;

    constructor(data: IServiceOrder) {
        this.businessUnit = data.businessUnit;
        this.serNumber = data.serNumber; // "SER#####"
        this.lineNumber = data.lineNumber; // "###"
        this.onsightType = data.onsightType; // "OS####"
        this.onsightDesc = data.onsightDesc; // OnSight type textual description
        this.soStatus = data.soStatus; // "OPA", "CFM", "COM", etc.
    
        this.promiseTime = data.promiseTime; // HH:mm, "AM", "PM", "Anytime", etc.
        this.startDate = data.startDate; // YYYY-MM-DD
        this.startTime = data.startTime; // HH:mm
        this.endDate = data.endDate; // YYYY-MM-DD
        this.endTime = data.endTime; // HH:mm

        this.clientCoId = data.clientCoId; 
        this.clientCoName = data.clientCoName;

        this.addrStreet = data.addrStreet; 
        this.addrCity = data.addrCity;
        this.addrState = data.addrState;
        this.addrZip = data.addrZip;

        this.contactName = data.contactName;
        this.contactPhone = data.contactPhone;
        this.contactAltPhone = data.contactAltPhone;
        this.contactEmail = data.contactEmail;

        this.iLogId = data.iLogId;
        this.iLogDate = data.iLogDate; // YYYY-MM-DDTHH:mm:ss.SSSSSSZZ
        this.iLogStatus = data.iLogStatus; // Empty string or something specific
        this.iLogDetails = data.iLogDetails;
    
        this.isTrunkwork = data.isTrunkwork;
        this.twAssocSerNumber = data.twAssocSerNumber; // Associated service order
        this.twDate = data.twDate; // YYYY-MM-DD
    
        this.inspectorId = data.inspectorId;
        this.inspectorFirstName = data.inspectorFirstName;
        this.inspectorLastName = data.inspectorLastName;

        this.hasPO = data.hasPO;
        this.poTechkey = data.poTechkey;
        this.poFileId = data.poFileId; // IDoc ID
    
        this.hasSI = data.hasSI;
        this.siFileId = data.siFileId; // IDoc ID
        
        this.occupancyCode = data.occupancyCode;
    }
}
*/

/*
export interface IInteractionLog {
    id?: number;

    soID: number; // IServiceOrder ID
    iLogId: string;
    iLogDate: Date; // YYYY-MM-DDTHH:mm:ss.SSSSSSZZ
    iLogStatus: string; // Empty string or something specific
    iLogDetails: string;
}

export class ServiceOrder {
    id?: number;
    serNumber: string;
    lineNumber: string;
    iLog?: IInteractionLog;

    constructor(serNumber: string, lineNumber: string, id?: number) {
        this.serNumber = serNumber;
        this.lineNumber = lineNumber;
        if (id) this.id = id;

        Object.defineProperties(this, {
            iLog: { value: null, enumerable: false, writable: true }
        })
    }

    save() {
        // TODO: Save service order and interaction log
        return db.transaction('rw', db.serviceOrders, db.interactionLogs, async () => {
            // Save service order
            this.id = await db.serviceOrders.put(this);

            // Save interaction log if available
            if (this.iLog) {
                await db.interactionLogs.put(this.iLog);
            }
        });
    }

    async loadInteractionLog() {
        if (this.id) {
            this.iLog = await db.interactionLogs.where('soID').equals(this.id).last();
        }
    }
}

export class OnSightDatabase extends Dexie {
    serviceOrders: Dexie.Table<ServiceOrder, number>;
    interactionLogs: Dexie.Table<IInteractionLog, number>;

    constructor() {
        super("OnSight");

        var db = this;

        db.version(1).stores({
            serviceOrders: '++id, [serNumber + lineNumber]',
            interactionLogs: '++id, soID'
        })

        db.serviceOrders.mapToClass(ServiceOrder);
    }
}

export var db = new OnSightDatabase();
*/

export interface IServiceOrder {
    id?: number;
    so: any; // JSON object
    siID?: number; // (FK) IDoc.id
    poID?: number; // (FK) IDoc.id

    rptID?: number; // (FK) iReport.id
    isNoReport?: boolean;
    isIncomplete: boolean;
    status: string;
    submissionDate?: Date
}

// export interface IReportSORef {
//     id?: number;
//     soID: number; // (FK) IServiceOrder.id
//     rptID?: number; // (FK) IReport.id
//     isNoReport?: boolean;
//     isIncomplete: boolean;
//     status: string;
//     submissionDate?: Date
// }

export interface IReport {
    id?: number;
    rpt: any; // JSON object

    /*
     * rpt object contains images' ID (IMedia.id)
     */
}

export interface IDoc {
    id?: number;
    filename: string;
    content: string; // Base64 encoded
}

export interface IMedia {
    id?: number;
    type: string;
    format: string;
    filename: string;
    caption: string;
    content: string; // Base64 encoded
}

const dbName = "OnSightDB";

const db = new Dexie(dbName);
db.version(1).stores({
    serviceOrders: '++id, &[so.serNumber+so.serLine], [so.startDate+so.startTime], status',
    reports: '++id',
    // soReportRef: '++id, status',
    documents: '++id',
    media: '++id'
});


export default db;
