import axios, { AxiosResponse } from 'axios';
import config from './app.json';

export interface IAccount {
    username: string,
    password: string
}
 
export interface IPSServiceOrder {
    BUSINESS_UNIT: string;
    SO_ID: string;
    SO_LINE: string;
    SOACTIVITY_STATUS: string;
    PERSON_ID: string;
    EST_START_DT: string;
    EST_START_TIME: string;
    EST_END_DT: string;
    EST_END_TIME: string;
    HSB_TRNK_WORK_FLG: string;
    HSB_TRNK_WORK_DT: string;
    TrunkWorkSOID: string;
    HSB_APPT_REQ_FLG: string;
    HSB_HARD_LOCK_FLG: string;
    ACTIVITY_CODE: string;
    DESCR80: string;
    SITE_ID: string;
    BO_NAME_DISPLAY: string;
    ADDRESS_150: string;
    CITY: string;
    STATE: string;
    POSTAL: string;
    BO_NAME_DISPLAY_1: string;
    PHONE: string;
    ALT_PHONE: string;
    INTERACTION_ID: number;
    INTERACT_BEGIN: Date;
    INTERACT_END: Date;
    HSB_VAL_TYPE_DESCR: string;
    NOTES: string;
    HSB_ACT_TECH_KEY: string;
    FIRST_NAME_SRCH: string;
    LAST_NAME_SRCH: string;
    BO_NAME_DISPLAY_2: string;
    HSB_CLIENT_CO_ID: string;
    BO_NAME_DISPLAY_3: string;
    HSB_PROMISE_DESCR: string;
    Special_Instr_Ind: string;
    OccupancyCode: string;
    EMAIL: string;
    VERSION: number;
}

export const authenticate = async (user: IAccount): Promise<AxiosResponse<any>> => {
    let url = `${config.host}${config.endpoints.authAPI}`;
    url = url.replace("{userId}", encodeURIComponent(user.username) );

    let axiosConfig = {
        headers: {
            "Ocp-Apim-Subscription-Key": config.key,
            "Ocp-Apim-Trace": true,
            "BII-Insp-Version": "2"
        },
        auth: {
            username: user.username,
            password: user.password
        }
    }

    const resp = await axios.get(url, axiosConfig);
    return resp;
};

/**
 * Transform raw service order to structured-data
 * @param {Object} rawData Raw service order
 * @return {Object}
 */
function transformRawSO(rawData: IPSServiceOrder) {
    var data = {
        promiseTime: rawData.HSB_PROMISE_DESCR,
        startDate: rawData.EST_START_DT,
        startTime: rawData.EST_START_TIME,
        endDate: rawData.EST_END_DT,
        endTime: rawData.EST_END_TIME,
        companyName: rawData.BO_NAME_DISPLAY_2,
        clientCompany: {
            id: rawData.HSB_CLIENT_CO_ID,
            name: rawData.BO_NAME_DISPLAY_3
        },
        site: {
            id: rawData.SITE_ID,
            name: rawData.BO_NAME_DISPLAY
        },
        businessUnit: rawData.BUSINESS_UNIT,
        address: {
            street: rawData.ADDRESS_150,
            city: rawData.CITY,
            state: rawData.STATE,
            zip: rawData.POSTAL
        },
        // contacts: [
        //     {
        //         name: rawData.BO_NAME_DISPLAY_1,
        //         phone: rawData.PHONE,
        //         altPhone: rawData.ALT_PHONE
        //     }
        // ],
        contact: {
            name: rawData.BO_NAME_DISPLAY_1,
            phone: rawData.PHONE,
            altPhone: rawData.ALT_PHONE
        },
        serNumber: rawData.SO_ID,
        serLine: rawData.SO_LINE,
        onsightType: rawData.ACTIVITY_CODE,
        type: rawData.DESCR80,
        serOrderStatus: rawData.SOACTIVITY_STATUS,
        status: "N",
        // interactionLogs: (function(id, date, status, notes) {
        //     if (id) {
        //         return [
        //             {
        //                 id: id,
        //                 date: new Date(date),
        //                 status: status,
        //                 details: [
        //                     notes
        //                 ]
        //             }
        //         ];
        //     } else {
        //         return [];
        //     }
        // })(rawData.INTERACTION_ID, rawData.INTERACT_BEGIN, rawData.HSB_VAL_TYPE_DESCR, rawData.NOTES),
        interactionLog: {
            id: rawData.INTERACTION_ID,
            date: rawData.INTERACT_BEGIN,
            status: rawData.HSB_VAL_TYPE_DESCR,
            details: rawData.NOTES
        },
        trunkWork: {
            state: (rawData.HSB_TRNK_WORK_FLG === 'Y'),
            assocSerNumber: rawData.TrunkWorkSOID,
            date: rawData.HSB_TRNK_WORK_DT
        },
        inspector: {
            id: rawData.PERSON_ID,
            firstName: rawData.FIRST_NAME_SRCH,
            lastName: rawData.LAST_NAME_SRCH
        },
        techKey: rawData.HSB_ACT_TECH_KEY,
        // priorOnsight: {
        //     state: null, // (rawData.PriorOnsightInd === "Y"),
        //     techKey: rawData.HSB_ACT_TECH_KEY,
        //     file: ""
        // },
        hasSI: (rawData.Special_Instr_Ind === "Y"),
        // specialInstruction: {
        //     state: (rawData.Special_Instr_Ind === "Y"),
        //     file: ""
        // },
        occupancyCode: rawData.OccupancyCode
    }

    if (config.version >= "1.5") {
        // Add email and form-version for v1.5+
        // Object.assign(data.contacts[0], data.contacts[0], {email: (rawData.EMAIL ? rawData.EMAIL : '')});
        Object.assign(data.contact, data.contact, { email: rawData.EMAIL });
        Object.assign(data, data, { version: rawData.VERSION || 1});

    }

    return data;
}

export const scheduleUpdate = async (inspectorID: string): Promise<AxiosResponse<any>> => {
    let url = `${config.host}${config.endpoints.scheduleUpdate}`;
    url = url.replace("{inspectorID}", encodeURIComponent(inspectorID) );

    let axiosConfig = {
        headers: {
            "Ocp-Apim-Subscription-Key": config.key,
            "Ocp-Apim-Trace": true,
            "BII-Insp-Version": "2"
        },
        transformResponse: [
            function(data: any): any {
                let json = JSON.parse(data);

                
                if (json.HSB_RESPONSE_SCHEDULE) {
                    // Add an empty array when ScheduleClass doesn't exist. i.e. Empty schedule
                    if (json.HSB_RESPONSE_SCHEDULE.ScheduleClass === undefined) {
                        json.HSB_RESPONSE_SCHEDULE.ScheduleClass = [];
                    }
                }

                console.log(json);
                let mappedData = json.HSB_RESPONSE_SCHEDULE.ScheduleClass.map((so: any) => {
                    return transformRawSO(so);
                })

                // return JSON.stringify(mappedData);
                return mappedData;
            }
        ]
    }

    const resp = await axios.get(url, axiosConfig);
    return resp;
};