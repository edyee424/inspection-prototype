import db from './db';


const addWorkorder = (so: any) => {
    db.transaction('rw', [db.table("serviceOrders")], async () => {

        db.table("serviceOrders").put({
            so: so,
            siID: null,
            poID: null,
            rptID: null,
            isNoReport: (so.onsightType === 'OS0001' ? false : null),
            isIncomplete: false,
            status: 'N',
            submissionDate: null
        }).then((value: any) => {
            console.log(`Inserted to serviceOrders ... ${value}`);

            // db.table("soReportRef").put({
            //     soID: value,
            //     rptID: null,
            //     isNoReport: (so.onsightType === 'OS0001' ? false : null),
            //     isIncomplete: false,
            //     status: 'N',
            //     submissionDate: null
            // }).then((value: any) => {
            //     console.log(`Inserted to soReportRef ... ${value}`);
            // });

        });

    });

}

const updateWorkorder = (soID: number, so: any) => {
    // db.transaction('rw', [db.table("serviceOrders")], async () => {
        db.table("serviceOrders").update(soID, { so: so });

    // });
};

const deleteWorkorder = (soID: number) => {
    // db.transaction('rw', [db.table("serviceOrders")], async () => {
        db.table("serviceOrders").update(soID, { status: 'D' });
    // });
};

const obsoleteWorkorder = (soID: number) => {
    db.table("serviceOrders").update(soID, { status: 'O' });
}

export const purgeWorkorders = () => {
    db.transaction('rw', [db.table("serviceOrders"), db.table("reports"), db.table("documents"), db.table("media")], async () => {
        // Find all purgable service orders
        db.table("serviceOrders")
            .where("status").equals("D")
            .each(so => {
                // For each service orders

                if (so.rptID) {
                    // Has report

                    // Get report
                    db.table("reports")
                        .where("id").equals(so.rptID)
                        .first()
                        .then(rpt => {
                            if (rpt.allowedPictures) {
                                // Delete corresponding media
                                db.table("media").bulkDelete(rpt.images);
                            }
                        });

                    // Delete report
                    db.table("reports").delete(so.rptID);
                }

                // Delete service order docs
                if (so.siID) {
                    db.table("documents").delete(so.siID);
                }

                if (so.poID) {
                    db.table("documents").delete(so.poID);
                }
            }).then(value => {
                // Delete service order and report
                db.table("serviceOrders")
                    .where("status").equals("D")
                    .delete();
            });
    });
};

export const processIncoming = (data: any[]) => {
    console.log(data);

    // db.table("soReportRef").update(1, { status: 'N' });

    data.forEach((so: any) => {
        db.transaction('rw', [db.table("serviceOrders"), db.table("reports"), db.table("documents"), db.table("media")], async () => {
            db.table("serviceOrders")
                .where("[so.serNumber+so.serLine]")
                .equals([so.serNumber, so.serLine])
                .and((so) => {
                    return ['N', 'I'].indexOf(so.status) >= 0;
                })
                .first()
                .then((value: any) => {
                    console.log(`Will update ${value.id}`);
                    updateWorkorder(value.id, so);

                }).catch(err => {
                    // Not in storage. Add it.
                    console.log(`Will add ${so.serNumber} (${so.serLine})`);
                    addWorkorder(so);
                });
        });
    });

    // Delete any service orders that are not in the incoming data
    db.transaction('rw', [db.table("serviceOrders")], async () => {
        db.table("serviceOrders")
            // .where("status").equals('N')
            .each(so => {
                var inIncoming = data.findIndex(iso => {
                    return iso.serNumber === so.so.serNumber && iso.serLine === so.so.serLine;
                });

                if (inIncoming < 0) {
                    if (so.status === 'N') {
                        deleteWorkorder(so.id);
                    } else if (['I', 'P'].indexOf(so.status) >= 0) {
                        // Change to obsolete
                        obsoleteWorkorder(so.id);
                    }
                }
            })
    });
};

/**
 * Get schedule
 * 
 * @return {object[]} List of service orders
 */
export const getSchedule = async () => {
    var schedule: any[] = [];
    
    // Order by start date and time
    // Filter by status "N" or "I"
    schedule = await db.table("serviceOrders").orderBy("[so.startDate+so.startTime]").filter(so => {
        return ['N', 'I'].indexOf(so.so.status) >= 0 && so.so.trunkWork.state === false;
    }).toArray();

    return schedule;
};