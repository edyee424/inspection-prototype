export const formatPhone = (src: string): string => {
    if (src.length === 10) {
        return `(${src.substr(0, 3)}) ${src.substr(3, 3)}-${src.substr(6)}`; 
    } else if (src.length === 7) {
        return `${src.substr(0,3)}-${src.substr(3)}`;
    }

    return src;
};