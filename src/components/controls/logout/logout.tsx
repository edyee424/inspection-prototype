import React from 'react';
import './logout.scss';

const Logout = (props: any) => {

    return (
        <div className={"smokeScreen " + (props.show ? "flex" : "hidden")} onClick={props.onNoClick}>
            <div className="inlineFlex dialogBox bg-White pa-sm box-shadow-br">
                <div className="font-size-md">
                    Do you really want to logout?
                </div>

                <div className="bottom-right mb-sm mr-sm">
                    <button className="noButton bg-secondary borderless text-White font-size-baseline border-radius-xs" onClick={props.onNoClick}>No</button>
                    <button className="yesButton bg-primary borderless text-White font-size-baseline border-radius-xs ml-xs" onClick={props.onYesClick}>Yes</button>
                </div>
            </div>
        </div>
    );
}

export default Logout;