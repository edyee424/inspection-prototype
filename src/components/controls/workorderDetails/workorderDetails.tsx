import React, { Fragment, useEffect, useState } from 'react';
import { Row, Col, Divider } from 'antd';
import Icon from '@mdi/react';
import { mdiClockStart, mdiClockEnd, mdiClose, mdiAccount, mdiPhone, mdiEmail, mdiNoteRemoveOutline } from '@mdi/js';
import dayjs from 'dayjs';

// Component styles
import './workorderDetails.scss';
import 'antd/dist/antd.css';

// Custom logics
import { formatPhone } from '../../../services/util';

export interface IWorkorderDetailsProps {
    selectedWorkorder: any,
    onCloseClick(e: React.MouseEvent<HTMLElement>): any;
    onEditClick?(e: React.MouseEvent<HTMLElement>): any;
    onNoReportClick?(e: React.MouseEvent<HTMLElement>): any;
    onIncompleteClick?(e: React.MouseEvent<HTMLElement>): any;
}

const WorkorderDetails = (props: IWorkorderDetailsProps) => {
    var wo = (props.selectedWorkorder ? props.selectedWorkorder.so : null);

    const [ showNoReportBtn, setShowNoReportBtn ] = useState(false);

    useEffect(() => {
        if (wo) {
            setShowNoReportBtn(wo.onsightType === 'OS0001');
        }
    }, [wo]);

    return (
        <div className="workorderDetails pa-xs">
            {wo ?
            <Fragment>
                <div className="text-right"><span onClick={props.onCloseClick}><Icon path={mdiClose} size={1.5}></Icon></span></div>
                <div>{wo.serNumber} ({wo.serLine})</div>
                <h1 className="siteName">{wo.site.name}</h1>
                <div className="font-size-baseline">{wo.address.street}<br />{wo.address.city}, {wo.address.state} {wo.address.zip}</div>

                <div className="dataGroup bg-LightGray box-shadow-br pa-xs mt-sm mr-xs">
                    <div className="text-Gray">Inspection Schedule</div>
                    <div>{dayjs(wo.startDate).format("MMMM d, YYYY")}</div>
                    <span className="dataWrapper inlineFlex valign-middle">
                        <Icon className="icon" path={mdiClockStart} /> {wo.startTime}
                    </span>
                    <span className="dataWrapper inlineFlex valign-middle ml-sm">
                        <Icon className="icon" path={mdiClockEnd} /> {wo.endTime}
                    </span>
                    {wo.promiseTime.length > 0 ?
                    <div>
                        Promise: {wo.promiseTime}
                    </div>
                    : null}

                </div>

                <div className="dataGroup bg-LightGray box-shadow-br pa-xs mt-sm mr-xs">
                    <div className="text-Gray">Site Contact</div>
                    <span className="dataWrapper inlineFlex valign-middle">
                        <Icon path={mdiAccount} className="icon" /> {wo.contact.name}
                    </span>
                    <div className="mt-xs">
                        <span className="dataWrapper inlineFlex valign-middle mr-sm">
                            <Icon path={mdiPhone} className="icon" /> {formatPhone(wo.contact.phone)}
                        </span>
                        {wo.contact.altPhone.length > 0 ? 
                        <Fragment>
                        <span className="dataWrapper inlineFlex valign-middle">
                            <Icon path={mdiPhone} className="icon" /> {formatPhone(wo.contact.altPhone)}
                        </span>
                        </Fragment>
                        : null}
                    </div>
                    <div className="mt-xs">
                        <span className="dataWrapper inlineFlex valign-middle">
                            <Icon path={mdiEmail} className="icon" /> {formatPhone(wo.contact.email)}
                        </span>
                    </div>
                </div>

                { wo.interactionLog.date ? 
                <Fragment>
                    <div className="dataGroup bg-LightGray box-shadow-br pa-xs mt-sm mr-xs">
                        <div className="text-Gray">Interaction Log</div>
                        <div>{dayjs(wo.interactionLog.date).format("MMMM d, YYYY")}</div>
                        <div>Status: {wo.interactionLog.status}</div>
                        <div>{wo.interactionLog.details}</div>
                    </div>
                </Fragment> 
                : null }

                <div className="mt-sm">
                    <button className="actionButton inlineFlex valign-middle halign-center bg-RoyalBlue text-GhostWhite primary">Start</button>
                    {showNoReportBtn ? 
                    <button className="actionButton inlineFlex valign-middle bg-Silver secondary"><Icon path={mdiNoteRemoveOutline} size={1} className="mr-xs"></Icon> No Report</button>
                    : null}
                    <button className="actionButton inlineFlex valign-middle bg-Silver secondary"><Icon path={mdiClose} size={1} className="mr-xs"></Icon> Incomplete</button>
                </div>
            </Fragment>
            : "There is no selected workorder" }
        </div>



    );
};

export default WorkorderDetails;
