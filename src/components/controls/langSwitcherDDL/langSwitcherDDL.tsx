import React, { Fragment, useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import Icon from '@mdi/react';
import { mdiMenuDown, mdiCheck } from '@mdi/js';

// Component styles
import './langSwitcherDDL.scss';

/*
 * English (Canada) en-CA
English (US) en-US
English (UK) en-UK
French (Canada) fr-CA
French (France) fr-FR
Chinese (&#38291;) zh-CN
Chinese (&#32321;) zh-HK
German de-DE
Spanish (Spain) es-ES
Spanish (Mexico) es-MX
 */

const LangSwitcherDDL = (props: any) => {
    const [t, i18n] = useTranslation();
    const baseLangListClasses = "langList box-shadow-bl";
    const baseSmokeScreenClasses = "fullscreen";
    const [ langListClasses, setLangListClasses ] = useState(baseLangListClasses + " hidden");
    const [ smokeScreenClasses, setSmokeScreenClasses ] = useState(baseSmokeScreenClasses + " hidden");
    const [ selectedLanguage, setSelectedLanguage ] = useState("");
    const [ showLangSelection, setShowLangSelection ] = useState(false);

    const supportedLang = useMemo(() => [
        { "locale": "en-CA", "caption": "English (Canada)" },
        { "locale": "en-US", "caption": "English (US)" },
        { "locale": "en-UK", "caption": "English (UK)" },
        { "locale": "fr-CA", "caption": "French (Canada)" },
        { "locale": "fr-FR", "caption": "French (France)" },
        { "locale": "zh-CN", "caption": "Simplified Chinese (簡)" },
        { "locale": "zh-HK", "caption": "Traditional Chinese (繁)" },
        { "locale": "de", "caption": "German" },
        { "locale": "es-ES", "caption": "Spanish (Spain)" },
        { "locale": "es-MX", "caption": "Spanish (Mexico)" }
    ], []);

    /**
     * Language DDL click event-handler
     * @param {React.MouseEvent<HTMLElement>} e 
     */
    const langDDLClick = (e: React.MouseEvent<HTMLElement>) => {
        if (langListClasses.indexOf("hidden") >= 0) {
            setShowLangSelection(true);
        } else {
            setShowLangSelection(false);
        }
    }

    /**
     * Change underlining language locale
     * @param {React.MouseEvent<HTMLElement>} e 
     * @param {string} locale Selected locale
     */
    const langClick = (e: React.MouseEvent<HTMLElement>, locale: string) => {
        console.log(e, locale);
        i18n.changeLanguage(locale);

        // Hide the selection box
        setShowLangSelection(false);
    }

    /**
     * Hide language selection
     * @param {React.MouseEvent<HTMLElement>} e 
     */
    const smokeScreenClick = (e: React.MouseEvent<HTMLElement>) => {
        setShowLangSelection(false);
    }

    // Change the DDL caption according to the selected locale
    useEffect(() => {
        let lang = supportedLang.find(l => {
            return l.locale === i18n.language;
        })

        if (lang === undefined) {
            lang = supportedLang.find(l => {
                return l.locale === "en-CA";
            })
        }

        setSelectedLanguage(lang?.caption || "English");
    }, [supportedLang, i18n.language])

    // Show or hide language selection box
    useEffect(() => {
        if (showLangSelection) {
            setLangListClasses(baseLangListClasses);
            setSmokeScreenClasses(baseSmokeScreenClasses);
        } else {
            setLangListClasses(baseLangListClasses + " hidden");
            setSmokeScreenClasses(baseSmokeScreenClasses + " hidden");
        }
    }, [showLangSelection])

    return (
        <div className="langSwitcherDDL">
            <div className={smokeScreenClasses} onClick={smokeScreenClick}></div>
            <div className="langSelectionWrapper">
    <div className="currentLang" onClick={langDDLClick}><span className="langLabel">{selectedLanguage}</span> <Icon path={mdiMenuDown} title="Select language" size={1.5} /></div>
                <ul className={langListClasses}>
                    {supportedLang.map(lang => {
                        return (
                        <li key={lang.locale} onClick={(e) => langClick(e, lang.locale)}>{lang.caption} <Icon path={mdiCheck} size={(localStorage.getItem("lang") === lang.locale ? 0.75 : 0)} /></li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};

export default LangSwitcherDDL;