import React, { Fragment } from 'react';
import Icon from '@mdi/react';
import { mdiClockStart, mdiClockEnd } from '@mdi/js';

import './workorderList.scss';

export interface IWorkorderListProps {
    schedule: object[];
    onItemClick?(e: React.MouseEvent<HTMLElement>, soID: number): any;
}

const WorkorderList = (props: IWorkorderListProps) => {

    const workorderItemClick = (e: React.MouseEvent<HTMLElement>, soID: number) => {

        if (props.onItemClick) {
            props.onItemClick(e, soID);
        }
    };

    return (
        <Fragment>
            { props.schedule.length > 0 ? 
                <ul className="listOfSchedule pl-none my-none">
                { props.schedule.map((so: any) => {
                    return (
                    <li className="pa-sm" key={so.id} onClick={(e) => workorderItemClick(e, so.id)}>
                        <span className="bold siteName">{so.so.site.name}</span><br />
                        {/* {so.so.serNumber} ({so.so.serLine})<br /> */}
                        <div className="mt-xs">
                            <span className="onsightType text-DarkSlateGray">{so.so.type}</span><br />
                            <div className="inspectionDatetime">
                                {so.so.startDate}
                                <Icon className="ml-xs bold text-Red" path={mdiClockStart} size={0.65} />{so.so.startTime}
                                <Icon className="ml-xs bold text-Red" path={mdiClockEnd} size={0.65} /> {so.so.endTime}
                            </div>
                        </div>
                    </li>);
    
                }) }
                </ul> 
            : 
                <div className="font-size-md pa-sm">
                    Empty schedule
                </div>
            }
        </Fragment>
    );
}

export default WorkorderList;