import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { Divider } from 'antd';
import Icon from '@mdi/react';
import { mdiMagnify, mdiFilterOutline, mdiBellOutline } from '@mdi/js';
import 'antd/dist/antd.css';

import './dashboardTopNav.scss';

export interface LeftMenuProps {
    onSearchClick?(e: React.MouseEvent<HTMLElement>): void,
    onFilterClick?(e: React.MouseEvent<HTMLElement>): void,
    onNotificationsClick?(e: React.MouseEvent<HTMLElement>): void,
    onAccountClick?(e: React.MouseEvent<HTMLElement>): void
}

const DashboardTopNav = (props: LeftMenuProps) => {
    const  [t] = useTranslation([ 'dashboard' ]);

    let auth = JSON.parse(localStorage.getItem("authtoken") || "?");
    let nameParts = auth.name.split(" ");
    let initials = nameParts[0].charAt(0) + nameParts[nameParts.length-1].charAt(0);

    const searchClick = (e: React.MouseEvent<HTMLElement>) => {
        if (props.onSearchClick) {
            props.onSearchClick(e);
        }
    };

    const filterClick = (e: React.MouseEvent<HTMLElement>) => {
        if (props.onFilterClick) {
            props.onFilterClick(e);
        }
    };

    const notificationsClick = (e: React.MouseEvent<HTMLElement>) => {
        if (props.onNotificationsClick) {
            props.onNotificationsClick(e);
        }
    };

    const accountClick = (e: React.MouseEvent<HTMLElement>) => {
        if (props.onAccountClick) {
            props.onAccountClick(e);
        }
    };


    return (
        <div className="dashboardTopNav flex h-spread valign-middle">
            <div className="all-caps bold title ml-sm font-size-md-@tablet font-size-baseline-@phone">
            <span className="pageTitle">{t("dashboard")}</span>
            </div>
            <div>
                <ul className="rightBlock">
                    <li onClick={searchClick}>
                        <Icon path={mdiMagnify} title="Search" className="buttonIcon" />
                        <div className="all-caps buttonLabel">Search</div>
                    </li>
                    <li onClick={filterClick}>
                        <Icon path={mdiFilterOutline} title="Filter" className="buttonIcon" />
                        <div className="all-caps buttonLabel">Filter</div>
                    </li>
                    <li onClick={notificationsClick}>
                        <Icon path={mdiBellOutline} title="Notifications" className="buttonIcon" />
                        <div className="all-caps buttonLabel">Notifications</div>

                    </li>
                    <li className="divider"></li>
                    <li onClick={accountClick}>
                        <span className="initials">{initials}</span>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default DashboardTopNav;