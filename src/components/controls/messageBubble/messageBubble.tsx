import React from 'react';
import './messageBubble.scss';

export enum MessageType {
    error = 1,
    warning
}

export interface MessageBubbleProps {
    type: MessageType,
    show: boolean,
    message: string
}

const MessageBubble = (props: MessageBubbleProps) => {
    let wrapperClasses = "messageBubbleWrapper " +  (props.show ? 'inlineBlock' : 'hidden');

    if (props.type === MessageType.error) {
        wrapperClasses += " error";
    }
    if (props.type === MessageType.warning) {
        wrapperClasses += " warning";
    }

    return (
        <div className={wrapperClasses}>
            <div className="pointer"></div>
            <div className="message pa-xs">{props.message}</div>
        </div>
    );
};

export default MessageBubble;