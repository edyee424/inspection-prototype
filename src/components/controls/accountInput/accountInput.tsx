import React, { useEffect, useRef } from 'react';
import './accountInput.scss';
import Icon from '@mdi/react';
import { mdiChevronRight } from '@mdi/js';

export interface AccountInputProps {
    buttonLabel: string,
    placeholder: string,
    onChange(e: React.ChangeEvent<HTMLElement>): void,
    onClick(e: React.MouseEvent<HTMLElement>): void,
    onKeyUp(e: React.KeyboardEvent<HTMLElement>): void,
    onKeyDown(e: React.KeyboardEvent<HTMLElement>): void,
    isPassword?: boolean,
    show: boolean,
    value?: any
}

const AccountInput = (props: AccountInputProps) => {
    let wrapperClasses = (props.show ? "flex" : "hidden");

    let inputType = 'text';
    if (props.isPassword !== undefined && props.isPassword) {
        inputType = 'password';
    }

    const inputRef = useRef<HTMLInputElement>(null);
    
    useEffect(() => {
        if (inputRef && inputRef.current) {
            inputRef.current.focus();
        }
    })

    return (
        <div className={"loginInput valign-middle " + wrapperClasses}>
            <input className="borderless login-input px-sm font-size-baseline-@phone font-size-baseline-@tablet" type={inputType} placeholder={props.placeholder} value={props.value} onChange={props.onChange} onKeyUp={props.onKeyUp} ref={inputRef} onKeyDown={props.onKeyDown} />
            <button className="all-caps borderless bg-Purple text-White login-button pl-sm pr-md font-size-baseline-@phone font-size-md-@tablet" onClick={props.onClick}><Icon path={mdiChevronRight} title={props.buttonLabel} size={1} className="hidden-@phone mr-sm-@tablet" /> <span className="button-label">{props.buttonLabel}</span></button>
        </div>
    );
};

export default AccountInput;