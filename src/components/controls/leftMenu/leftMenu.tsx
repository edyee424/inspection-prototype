import React, { Fragment, useState } from 'react';
import Icon from '@mdi/react';
import { mdiAccountHardHat, mdiCalendarMonth, mdiInformation, mdiSync, mdiCalendarSync } from '@mdi/js';

import './leftMenu.scss';

export interface LeftMenuProps {
    className?: string;
    selectedMenuItem?: string;
    onWorkordersClick?(e: React.MouseEvent<HTMLElement>): Promise<any>,
    onScheduleClick?(e: React.MouseEvent<HTMLElement>): Promise<any>,
    onSyncClick?(e: React.MouseEvent<HTMLElement>, postFn: () => any): Promise<any>,
    onAboutClick?(e: React.MouseEvent<HTMLElement>): any,
}

const LeftMenu = (props: LeftMenuProps) => {
    const [ selectedItem, setSelectedItem ] = useState(props.selectedMenuItem ?? "");

    const [ syncIcon, setSyncIcon ] = useState({ icon: mdiCalendarSync, spin: 0 });

    const workordersClick = async (e: React.MouseEvent<HTMLElement>) => {
        // Pre-code
        setSelectedItem("workorders");

        if (props.onWorkordersClick) {
            await props.onWorkordersClick(e);
        }

        // Post-code
    };

    const scheduleClick = async (e: React.MouseEvent<HTMLElement>) => {
        // Pre-code
        setSelectedItem("schedule");

        if (props.onScheduleClick) {
            await props.onScheduleClick(e);
        }

        // Post-code
    }

    const syncClick = async (e: React.MouseEvent<HTMLElement>) => {
        setSyncIcon({ icon: mdiSync, spin: 2 });

        // The calling program will provide a function to perform synchronization.
        // This handler will not know when to change the icon. So it needs to pass
        // a change-icon callback function when executing the synchronization
        // function.
        if (props.onSyncClick) {
            await props.onSyncClick(e, () => { setSyncIcon({ icon: mdiCalendarSync, spin: 0 }) });
        }
    };

    const aboutClick = async (e: React.MouseEvent<HTMLElement>) => {
        // Pre-code

        if (props.onAboutClick) {
            await props.onAboutClick(e);
        }

        // Post-code
    }

    return (
        <Fragment>
            <div className={"leftMenu " + props.className}>
                <ul className="mt-md mt-none-@phone mb-none pl-none">
                    <li className={selectedItem === "workorders" ? "active" : ""} onClick={workordersClick}>
                        <Icon path={mdiAccountHardHat} title="Workorders" size={1.5} />
                        <div className="all-caps">WorkOrders</div>
                    </li>
                    <li className={selectedItem === "schedule" ? "active" : ""} onClick={scheduleClick}>
                        <Icon path={mdiCalendarMonth} title="Schedule" size={1.5} />
                        <div className="all-caps">Schedule</div>
                    </li>
                    <li onClick={syncClick}>
                        <Icon path={syncIcon.icon} title="Sync Calendar" size={1.5} spin={syncIcon.spin} />
                        <div className="all-caps">Sync</div>
                    </li>
                    <li onClick={aboutClick}>
                        <Icon path={mdiInformation} title="About" size={1.5} />
                        <div className="all-caps">About</div>
                    </li>
                </ul>
            </div>

        </Fragment>
    );
}

export default LeftMenu;