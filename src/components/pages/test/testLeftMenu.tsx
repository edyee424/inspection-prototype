import React, { Fragment } from 'react';
import LeftMenu from '../../controls/leftMenu/leftMenu';

const TestLeftMenu = () => {
    const mockSyncClick = async (e: React.MouseEvent<HTMLElement>, endSyncCallback: () => any) => {
        return new Promise((resolve, reject) => {
            console.log("mock sync click handler");

            setTimeout(() => {
                console.log("Call endSyncCallback");
                endSyncCallback();
                resolve();
            }, 5000);
        })
    };

    const workordersClick = async (e: React.MouseEvent<HTMLElement>) => {
        return new Promise((resolve, reject) => {
            console.log("workorders click handler")
        })
    };

    const scheduleClick = async (e: React.MouseEvent<HTMLElement>) => {
        return new Promise((resolve, reject) => {
            console.log("schedule click handler")
        })
    };

    const aboutClick = async (e: React.MouseEvent<HTMLElement>) => {
        return new Promise((resolve, reject) => {
            console.log("about click handler")
        })
    };

    return (
        <Fragment>
            <LeftMenu onSyncClick={mockSyncClick} onWorkordersClick={workordersClick} onScheduleClick={scheduleClick} onAboutClick={aboutClick} />
        </Fragment>
    );
}

export default TestLeftMenu;