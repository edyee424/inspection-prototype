import React, { Fragment } from 'react';
import DashboardTopNav from '../../controls/dashboardTopNav/dashboardTopNav';

const TestDashboardTopNav = () => {
    const accountClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("Account clicked");
    }

    const searchClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("Search clicked");
    }

    const filterClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("Filter clicked");
    }

    const notificationsClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("Notifications clicked");
    }

    return (
        <Fragment>
            <DashboardTopNav onAccountClick={accountClick} onFilterClick={filterClick} onNotificationsClick={notificationsClick} onSearchClick={searchClick} />
        </Fragment>
    );
}

export default TestDashboardTopNav;