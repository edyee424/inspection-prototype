import React, { useEffect, useState } from 'react';
import { getSchedule } from '../../../services/schedule';
import WorkorderList from '../../controls/workorderList/workorderList';
import './testWorkorderList.scss';

const TestWorkorderList = () => {
    const [ schedule, setSchedule ] = useState<any>([]);

    const woItemClick = (e: React.MouseEvent<HTMLElement>, soID: number) => {
        console.log(soID);
    };

    useEffect(() => {
        getSchedule().then(data => {
            setSchedule(data);
        });
    })

    return (
        <div className="workOrders">
            <WorkorderList schedule={schedule} onItemClick={woItemClick}></WorkorderList>
        </div>
    );
}

export default TestWorkorderList;