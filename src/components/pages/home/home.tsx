import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

// Component styles
import "./home.scss";

// Custom logics
import { scheduleUpdate } from '../../../services/azure';
import { processIncoming, getSchedule } from '../../../services/schedule';

// Custom components
// import LangSwitcher from "../../controls/langSwitcher/langSwitcher";
import Logout from "../../controls/logout/logout";
import WorkorderDetails from "../../controls/workorderDetails/workorderDetails";
import LeftMenu from "../../controls/leftMenu/leftMenu";
import DashboardTopNav from "../../controls/dashboardTopNav/dashboardTopNav";
import WorkorderList from "../../controls/workorderList/workorderList";

const Home = (props: any) => {
    const  [t] = useTranslation([ 'dashboard' ]);
    let auth = JSON.parse(localStorage.getItem("authtoken") || "?");
    let nameParts = auth.name.split(" ");
    let initials = nameParts[0].charAt(0) + nameParts[nameParts.length-1].charAt(0);

    // const [ syncIcon, setSyncIcon ] = useState({ icon: mdiCalendarSync, spin: 0 });
    const [ showLogout, setShowLogout ] = useState(false);
    const [ schedule, setSchedule ] = useState<any>([]);

    const [ selectedWorkorder, setSelectedWorkorder ] = useState(null);
    const [ slideCss, setSlideCss ] = useState('');
    const [ showWODetailsPhone, setShowWODetailsPhone ] = useState('hidden-@tablet')
    const [ showWODetails, setShowWODetails ] = useState('hidden');

    /**
     * Sync button (left-nav) click event-handler
     * @param {React.MouseEvent<HTMLElement>} e 
     * @param {function} endSyncCallback A callback function to finish the component behaviour
     */
    const syncClick = async (e: React.MouseEvent<HTMLElement>, endSyncCallback: () => any) => {

        return new Promise((resolve, reject) => {
            var user = JSON.parse(localStorage.getItem("authtoken") ?? "{ id: 0, name: '' }");

            scheduleUpdate(user.id).then(
                function(resp) {
                    console.log(resp);
                    processIncoming(resp.data);
                    resolve();
                }
            ).catch(
                function(error) {
                    console.error(error);
                }
            ).finally(() => {
                if (endSyncCallback) {
                    endSyncCallback();
                }
            })
        })

    };

    /**
     * User initials (top-nav) click event-handler.
     * @param {React.MouseEvent<HTMLElement>} e 
     */
    const initialsClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("logoff clicked")
        setShowLogout(true);
    }

    const aboutClick = (e: React.MouseEvent<HTMLElement>) => {
        // setShowWODetailsPhone('slideRight');
        // setSlideCss('slideRight');
    }

    /**
     * Logout modal No button click event-handler
     * @param {React.MouseEvent<HTMLElement>} e 
     */
    const logoutNoBtnClick = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();

        console.log("Stay on!");
        setShowLogout(false);
    }

    /**
     * Logout modal Yes button click event-handler
     * @param {React.MouseEvent<HTMLElement>} e 
     */
    const logoutYesBtnClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log("Logoff!");
        setShowLogout(false);
        localStorage.removeItem("authtoken");

        // Navigate to login screen
        props.history.push('/Login');
    }

    const woItemClick = (e: React.MouseEvent<HTMLElement>, soID: number) => {
        // e.preventDefault();
        setShowWODetails("");
        setShowWODetailsPhone('slideLeft');
        setSlideCss('slideLeft');

        var subset = schedule.filter((wo: any) => {
            return wo.id === soID;
        })

        setSelectedWorkorder(subset[0]);
    }

    const woDetailsCloseClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log('close')
        setShowWODetails("hidden-@tablet");
        setShowWODetailsPhone('slideRight');
        setSlideCss('slideRight');

    }

    useEffect(() => {
        getSchedule().then(data => {
            setSchedule(data);
        });
    })


    return (
        <div className="full-screen dashboard">
            <div className="bg-toronto"></div>
            
            <div className="left-nav full-height">
                <LeftMenu onSyncClick={syncClick} onAboutClick={aboutClick} />
            </div>

            <div className="top-nav py-xs-@phone">
                <DashboardTopNav onAccountClick={initialsClick} />
            </div>

            <div className={"workOrders " + slideCss}>
                <WorkorderList schedule={schedule} onItemClick={woItemClick}></WorkorderList>
            </div>

            <div className={`woDetails ${showWODetailsPhone} ${showWODetails}`}>
                <WorkorderDetails selectedWorkorder={selectedWorkorder} onCloseClick={woDetailsCloseClick} />
            </div>

            <Logout show={showLogout} onYesClick={logoutYesBtnClick} onNoClick={logoutNoBtnClick} />


        </div>
    );
};

export default withRouter(Home);