import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

// Component styles
import './login.scss';

// Custom logics
import { authenticate } from '../../../services/azure';

// Custom components
import AccountInput from '../../controls/accountInput/accountInput';
// import MessageBubble, { MessageType } from '../../controls/messageBubble/messageBubble';
import LangSwitcherDDL from '../../controls/langSwitcherDDL/langSwitcherDDL';

const Login = (props: any) => {
    const  [t] = useTranslation([ 'login' ]);
    const [account, setAccount] = useState({
        username: '',
        password: ''
    });
    const [errorMessage, setErrorMessage] = useState({
        show: false,
        message: ''
    });
    const [showUsernameInput, setShowUsernameInput] = useState(true);
    const [showPasswordInput, setShowPasswordInput] = useState(false);

    /**
     * Username input change event event-handler
     * @param event [Event] Changed event object
     */
    const usernameChange = (event: any) => {
        setAccount({
            username: event.target.value,
            password: account.password
        });
    };

    /**
     * Username button click event event-handler
     * @param event [Event] Click event object
     * @param event 
     */
    const usernameClick = (event: any) => {
        usernameProceed();
    };

    /**
     * Password input change event event-handler
     * @param event [Event] Changed event object
     */
    const passwordChange = (event: any) => {
        setAccount({
            username: account.username,
            password: event.target.value
        });
    };

    /**
     * Password button click event event-handler
     * @param event [Event] Click event object
     */
    const passwordClick = (event: any) => {
        passwordProceed();
    };

    /**
     * 
     */
    const usernameProceed = () => {
        // Hide username AccountInput. Show password AccountInput

        if (account.username.length === 0) {
            setErrorMessage({ show: true, message: t("enterUserName.errors.invalid")});
        } else {
            setShowUsernameInput(false);
            setShowPasswordInput(true);
        }

    }

    /**
     * Authenticate user inputs.
     */
    const passwordProceed = () => {
        if (account.password.length === 0) {
            setErrorMessage({ show: true, message: t("enterPassword.errors.invalid")});
        } else {
            // Clear current error message
            setErrorMessage({ show: false, message: "" });

            // TODO: Authenticate account. If fails, display error message.
            authenticate(account).then(function(resp) {
                // console.log(resp);
                let data = resp.data;

                if (data.HSB_CheckUser_Resp.RESULT === "FAIL") {
                    setErrorMessage({ show: true, message: data.HSB_CheckUser_Resp.ERROR.error })

                } else {
                    localStorage.setItem("authtoken", JSON.stringify({ id: data.HSB_CheckUser_Resp.person_id, name: data.HSB_CheckUser_Resp.name }));

                    // Navigate to home screen
                    props.history.push('/Home');
                }
            }).catch(function(error) {
                // console.error('In login.tsx', error);

                setAccount({
                    username: '',
                    password: ''
                });

                setShowUsernameInput(true);
                setShowPasswordInput(false);
    
                if (error.response.status && error.response.status === 401) {

                    setErrorMessage({ show: true, message: t("httpErrors.401") })

                } else {
                    setErrorMessage({ show: true, message: t("httpErrors.500") })
                }

            })
            
        }
        
    }

    /**
     * Common input field key-up event-handler
     * @param {React.KeyboardEvent<HTMLElement>} event 
     */
    const keyUp = (event: React.KeyboardEvent<HTMLElement>) => {
        // Hide previous error message
        setErrorMessage({
            show: false,
            message: ''
        });
    }

    /**
     * Handle username input Enter key
     * @param {React.KeyboardEvent<HTMLElement>} event 
     */
    const usernameKeyDown = (event: React.KeyboardEvent<HTMLElement>) => {
        // Process Enter key press for username
        if (event.key === "Enter") {
            usernameProceed();
        }
    }

    /**
     * Handle password input Enter key
     * @param {React.KeyboardEvent<HTMLElement>} event 
     */
    const passwordKeyDown = (event: React.KeyboardEvent<HTMLElement>) => {
        if (event.key === "Enter") {
            passwordProceed();
        }
    }

    return (
        <div className="full-screen login">
            <div className="toronto-bg"></div>
            <div className="white-pane">
            <div className="login-container py-sm pl-sm-@phone pl-md">
                <div className="all-caps font-condensed bold font-size-lg-@tablet font-size-md-@phone">{t("getStarted")}</div>
                <div className="text-DarkSlateGray font-size-md-@tablet font-size-baseline-@phone mb-lg mt-xs">{t("signIn")}</div>

                <AccountInput placeholder={t("enterUserName.placeholder")} buttonLabel={t("enterUserName.buttonLabel")} onChange={usernameChange} onClick={usernameClick} onKeyUp={keyUp} onKeyDown={usernameKeyDown} show={showUsernameInput} value={account.username} />
                <AccountInput placeholder={t("enterPassword.placeholder")} buttonLabel={t("enterPassword.buttonLabel")} onChange={passwordChange} onClick={passwordClick} onKeyUp={keyUp} onKeyDown={passwordKeyDown} show={showPasswordInput} isPassword={true} value={account.password} />

                <div className="text-Red mt-sm">{errorMessage.message}</div>
            </div>
            <div className="langSwitcher">
                <LangSwitcherDDL />
            </div>

            </div>

            {/* <MessageBubble type={MessageType.error} show={errorMessage.show} message={errorMessage.message} /> */}

        </div>


    );
};

export default withRouter(Login);