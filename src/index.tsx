import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// Bilingual support
import './i18n';

// Component style
import './index.css';

// Custom components
import Login from './components/pages/login/login';
import Home from './components/pages/home/home';
import TestLeftMenu from './components/pages/test/testLeftMenu';
import TestDashboardTopNav from './components/pages/test/testDashboardTopNav';
import TestWorkorderList from './components/pages/test/testWorkorderList';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Suspense fallback="Loading ....">
        <App />
        <Switch>
          <Route path="/Login" component={Login} />
          <Route path="/Home" component={Home} />
          <Route path="/TestLeftMenu" component={TestLeftMenu} />
          <Route path="/TestDashboardTopNav" component={TestDashboardTopNav} />
          <Route path="/TestWorkorderList" component={TestWorkorderList} />
        </Switch>

      </Suspense>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
